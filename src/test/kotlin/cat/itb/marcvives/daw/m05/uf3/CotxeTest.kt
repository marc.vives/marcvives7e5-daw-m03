package cat.itb.marcvives.daw.m05.uf3

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class CotxeTest {

    @Test
    fun anysMatricultaTest() {
        val cotxeTest = Cotxe("1010BBB", "Panda", 2005)
        assertEquals(17, cotxeTest.anysMatriculat())
    }

    @Test
    fun validarMatriculaCorrecta() {
        val cotxeTest = Cotxe("1010BBB", "Panda", 2005)
        assertTrue(cotxeTest.validarMatricula())
    }

    @Test
    fun validarMatriculaNoValidaVocals() {
        val cotxeTest = Cotxe("1010AAA", "Panda", 2005)
        assertFalse(cotxeTest.validarMatricula())
    }

    @Test
    fun validarMatriculaNoValidaOrder() {
        val cotxeTest = Cotxe("BBB1010", "Panda", 2005)
        assertFalse(cotxeTest.validarMatricula())
    }

    @Test
    fun validarMatriculaNoValidaLongitudPetita() {
        val cotxeTest = Cotxe("BB10", "Panda", 2005)
        assertFalse(cotxeTest.validarMatricula())
    }

    @Test
    fun validarMatriculaNoValidaLongitudGran() {
        val cotxeTest = Cotxe("BB101010", "Panda", 2005)
        assertFalse(cotxeTest.validarMatricula())
    }

}