package cat.itb.marcvives.daw.m05.uf3

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import java.util.*

internal class AlumneTest{

    @Test
    fun nomCompletTest(){
        val expected = "nom, cognom1 cognom2"
        val alumneTest = Alumne("nom", "cognom1 cognom2", Date(1990,12,30))
        assertEquals(expected, alumneTest.nomComplet())
    }
}