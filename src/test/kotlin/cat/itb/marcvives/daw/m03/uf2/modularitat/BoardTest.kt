package cat.itb.marcvives.daw.m03.uf2.modularitat

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.fail

internal class BoardTest {

    @Test
    fun generateBombsListSize() {
        val n = 5
        val numBombes = 15
        val tauler = Board(n, numBombes)
        assertEquals(numBombes, tauler.generateBombsList().size)
    }

    @Test
    fun generateBombListUniques() {
        val n = 5
        val numBombes = 15
        val tauler = Board(n, numBombes)
        val llistaBombes = tauler.generateBombsList()
        val aux = mutableListOf<Cell>()
        var repetida = false
        for (cell in llistaBombes) {
            if (aux.contains(cell)) {
                repetida = true
                break
            } else {
                aux.add(cell)
            }
        }
        assertFalse(repetida)
    }


    @Test
    fun calculateAdjacenciesAtCorner(){
        val n = 5
        val numBombes = n*n
        val tauler = Board(n, numBombes)
        tauler.addBombs(tauler.generateBombsList())
        tauler.calculateAdjacencies()
        val adjacentsEsperades = 3
        assertEquals(adjacentsEsperades, tauler.matrix[0][0].adjacents)
    }

    @Test
    fun calculateAdjacenciesAtBorder(){
        val n = 5
        val numBombes = n*n
        val tauler = Board(n, numBombes)
        tauler.addBombs(tauler.generateBombsList())
        tauler.calculateAdjacencies()
        val adjacentsEsperades = 5
        assertEquals(adjacentsEsperades, tauler.matrix[0][1].adjacents)
    }

    @Test
    fun calculateAdjacenciesAtCenter(){
        val n = 5
        val numBombes = n*n
        val tauler = Board(n, numBombes)
        tauler.addBombs(tauler.generateBombsList())
        tauler.calculateAdjacencies()
        val adjacentsEsperades = 8
        assertEquals(adjacentsEsperades, tauler.matrix[1][1].adjacents)
    }

    @Test
    fun showInformation(){
        val n = 3
        val numBombes = 15
        val tauler = Board(n, numBombes)
        val resultatEseperat = "X X X \nX X X \nX X X \n"
        assertEquals(resultatEseperat, tauler.showInformation())
    }
}