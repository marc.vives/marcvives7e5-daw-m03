package cat.itb.marcvives.daw.m03.uf2.modularitat

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class CellTest{
    @Test
    fun showIniticalAttributes(){
        val fila = 3
        val columna = 5
        val cell = Cell(fila, columna)

        assertFalse(cell.bomba)
        assertFalse(cell.destapada)
        assertEquals(fila, cell.i)
        assertEquals(columna, cell.j)
    }

    @Test
    fun showInformationIntial(){
        val fila = 3
        val columna = 5
        val cell = Cell(fila, columna)
        assertEquals("X", cell.showInformation())
    }

}