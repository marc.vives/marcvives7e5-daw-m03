package cat.itb.marcvives.daw.m03.uf2.recursivity

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class MatesKtTest{

    @Test
    fun multiplicaEntersPositius(){
        assertEquals(6, multiplicaFake(2,3))
    }

    @Test
    fun multiplicaEntersNegatius(){
        assertEquals(6, multiplicaFake(-2,-3))
    }

    @Test
    fun multiplicaZeroIPositiu(){
        assertEquals(0, multiplicaFake(0,3))
    }
    @Test
    fun multiplicaNegatiuIPositiu(){
        assertEquals(-5, multiplicaFake(-1,5))
    }
    @Test
    fun multiplicaPositiuINegatiu(){
        assertEquals(-5, multiplicaFake(5,-1))
    }
}