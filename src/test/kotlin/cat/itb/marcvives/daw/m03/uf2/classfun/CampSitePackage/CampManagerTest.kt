package cat.itb.marcvives.daw.m03.uf2.classfun.CampSitePackage

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.fail

internal class CampManagerTest{
    @Test
    fun registrarSortidaUnicaReserva(){
        val reserva = Reserva("Marc", 5)
        val campManager = CampManager(mutableListOf(reserva))
        campManager.registrarSortida("Marc")

        assertEquals(0, campManager.reserves.size)
    }

    //TODO: desarrollar prueba
    @Test
    fun registrarSortidaTestNoExisteix(){
        fail("not yet implemented")
    }


    @Test
    fun validarNomsReserves(){
        val reserva1 = Reserva("Marc", 5)
        val reserva2 = Reserva("Oriol", 5)
        val reserva3 = Reserva("Roger", 5)

        val reserves = mutableListOf(reserva1, reserva2, reserva3)
        val campManager = CampManager(reserves)

        campManager.registrarSortida(reserva2.nombre)
        val nomsEsperats = listOf(reserva1.nombre, reserva3.nombre)
        assertEquals(nomsEsperats, campManager.nomsReserva())
    }
}