package cat.itb.marcvives.daw.m03.uf2.classfun

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class RightTriangleTest{
    @Test
    fun creacionDeRightTriangle(){
        val triangle = RightTriangle(4.0, 2.0)
        assertEquals(4.0,triangle.base)
        assertEquals(2.0,triangle.altura)
    }

    @Test
    fun validarAreaRightTriangle(){
        val triangle = RightTriangle(4.0, 2.0)
        assertEquals(4.0, triangle.area)
    }
}