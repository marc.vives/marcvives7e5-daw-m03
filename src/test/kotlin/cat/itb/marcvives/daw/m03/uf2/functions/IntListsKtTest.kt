package cat.itb.marcvives.daw.m03.uf2.functions

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class IntListsKtTest {

    @Test
    fun testMinimNegatius() {
        val list = listOf(-5, -10, -1, -1)
        val expectedMin = -10
        val minCalculado = min(list)
        assertEquals(expectedMin, minCalculado)
    }

    @Test
    fun testMinimPositius() {
        val list = listOf(5, 10, 1, 1)
        val expectedMin = 1
        val minCalculado = min(list)
        assertEquals(expectedMin, minCalculado)
    }
}