package cat.itb.marcvives.daw.m05.uf3


data class Person(var name: String, var surnames: String) {
    fun fullName():String{
        return "$name $surnames"
    }
}


fun main() {
    val persona = Person("Alba", "Abella")
    println(persona.name)
    persona.name = "Alba Maria"
}