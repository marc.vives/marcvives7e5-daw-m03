package cat.itb.marcvives.daw.m05.uf3

data class Cotxe(val matricula: String, val model: String, val anyMatriculacio: Int) {
    fun anysMatriculat(): Int {
        return 0
    }

    fun validarMatricula(): Boolean {
        return false
    }
}
