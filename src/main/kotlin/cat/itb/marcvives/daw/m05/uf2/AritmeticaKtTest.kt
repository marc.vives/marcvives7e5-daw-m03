package cat.itb.marcvives.daw.m05.uf2

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class AritmeticaKtTest {

    @Test
    fun sumTestPositivos() {
        assertEquals(5, sum(2,3))
    }
    @Test
    fun sumTestNegativos() {
        assertEquals(-5, sum(-2,-3))
    }

    @Test
    fun divTestAmayorB(){
        val a = 5
        val b = 3
        val expected = 1
        assertEquals(expected, division(a, b))
    }

    @Test
    fun divTestBmayorA(){
        val a = 3
        val b = 8
        val expected = 0
        assertEquals(expected, division(a, b))
    }

    @Test
    fun divTestBigual0() {

    }
}