package cat.itb.marcvives.daw.m03.uf4.exercices

class Board(val rectangles: List<Rectangle>){

    fun getTotalArea(): Double {
        var area = 0.0
        for (rectangle in rectangles) {
            area += rectangle.area
        }
        return area
    }

    fun countRectangles(): Int {
        return rectangles.size
    }
}