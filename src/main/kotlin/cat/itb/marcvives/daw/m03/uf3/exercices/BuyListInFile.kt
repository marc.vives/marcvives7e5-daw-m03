package cat.itb.marcvives.daw.m03.uf3.exercices

import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.nio.file.Path
import java.util.*
import kotlin.io.path.Path
import kotlin.io.path.exists
import kotlin.io.path.readText
import kotlin.io.path.writeText

fun main() {
    val sc = Scanner(System.`in`).useLocale(Locale.UK)
    val homePath = Path(System.getProperty("user.home"))
    val compraFile = homePath.resolve("shopingList.json")

    var productes = mutableListOf<Product>()

    if(compraFile.exists()){
        productes = getProductsFromJsonFile(compraFile)
    }

    productes.add(getProductFromScanner(sc))

    printShopingList(productes)
    saveListProductInJsonFile(productes, compraFile)
}

fun saveListProductInJsonFile(products: List<Product>, pathFile: Path){
    val productsJson = Json.encodeToString(products) //
    if(pathFile.exists()){
        println("Se sobreescriu el fitxer $pathFile")
    }
    pathFile.writeText(productsJson)
}

fun getProductsFromJsonFile(pathFile: Path): MutableList<Product>{
    val productsJson = pathFile.readText()    // contenido del fichero
    val products : MutableList<Product> = Json.decodeFromString(productsJson)
    return products
}

fun printShopingList(products : MutableList<Product>){
    println("-------- Compra --------")
    products.forEach{ product->
        println("${product.quantity} ${product.name} (${product.price}€) - ${product.totalPrice}€")
    }
    println("-------------------------")
    println("Total: ${shopingListTotalPrice(products)}€")
    println("-------------------------")
}

fun shopingListTotalPrice(products : MutableList<Product>): Double{
    var total = 0.0
    products.forEach { product ->
        total += product.totalPrice
    }
    return total
}

fun getProductFromScanner(lector : Scanner):Product{
    val quantitat = lector.nextInt()
    val nom = lector.next()
    val preu = lector.nextDouble()
    lector.nextLine()
    return Product(nom, preu, quantitat)
}

data class Product(val name: String, var price: Double, var quantity: Int){
    val totalPrice get() = quantity * price
}