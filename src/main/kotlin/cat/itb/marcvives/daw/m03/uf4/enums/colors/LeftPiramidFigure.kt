package cat.itb.marcvives.daw.m03.uf4.enums.colors

class LeftPiramidFigure(color: PrintColors, val base: Int) : Figura(color){

    override fun print(){
        super.prepareColor()

        for(piso in 0..base){
            repeat(piso){
                print("X")
            }
            println()
        }
        println()

        super.clearColor()
    }
}