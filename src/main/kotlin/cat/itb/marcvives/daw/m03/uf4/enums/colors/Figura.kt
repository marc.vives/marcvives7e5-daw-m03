package cat.itb.marcvives.daw.m03.uf4.enums.colors

open class Figura(val color: PrintColors) {

    open fun print(){}

    fun prepareColor(){
        print(color.consoleColor)
    }

    fun clearColor(){
        print("\u001b[0m")
    }
}