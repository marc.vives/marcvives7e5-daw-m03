package cat.itb.marcvives.daw.m03.uf1.iterative

import java.util.*

fun main() {
    val lector = Scanner(System.`in`)
    val final = lector.nextInt()
    val salt = lector.nextInt()
    val imprimir = countWithJumps(final, salt)
    println(imprimir)
}

fun countWithJumps(final: Int, salt: Int): String {
    var resultat: String = ""
    for (i in 1..final step salt) {
        resultat += "$i "
    }
    return resultat
}