package cat.itb.marcvives.daw.m03.uf1.lists

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)

    val listaPalabras = mutableListOf("primera","segona")
    val listAdicional = MutableList(3){
        scanner.next()
    }

    println("listaPalabras = ${listaPalabras}")
    println("listAdicional = ${listAdicional}")

    //val terceraLista = listaPalabras + listAdicional
    listaPalabras.addAll(listAdicional)
    println("listaPalabras = ${listaPalabras}")
    println("listAdicional = ${listAdicional}")


}