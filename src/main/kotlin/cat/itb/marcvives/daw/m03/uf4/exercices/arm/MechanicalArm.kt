package cat.itb.marcvives.daw.m03.uf4.exercices.arm

class MechanicalArm(
    var openAngle: Double = 0.0,
    var altitude: Double = 0.0,
    var estat: Boolean = false
) {

    fun toggle() {
        estat = !estat
    }

    fun updateAltitude(altitude: Int) {
        val newAltitude = this.altitude + altitude
        if (estat && altitudeAvaible(newAltitude)) {
            this.altitude = newAltitude
        }
    }

    fun updateAngle(angle: Int) {
        val newAngle = this.openAngle + angle
        if (estat && angleAvaible(newAngle)) {
            this.openAngle = newAngle
        }
    }

    private fun altitudeAvaible(newAltitude: Double): Boolean {
        return newAltitude >= 0
    }

    private fun angleAvaible(newAngle: Double): Boolean {
        return (newAngle >= 0 && newAngle <= 360)
    }

    fun getStatus(): String {
        return "MechanicalArm{openAngle=$openAngle, altitude=$altitude, estat=$estat}"
    }

}