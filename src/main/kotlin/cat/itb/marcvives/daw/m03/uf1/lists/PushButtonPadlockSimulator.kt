package cat.itb.marcvives.daw.m03.uf1.lists

import java.util.*

fun main() {
    val lector = Scanner(System.`in`)
    val candau : MutableList<Boolean>
    candau = MutableList(8){ false }

    var boto = lector.nextInt()

    while(boto != -1){
        //inverteixo el valor del boto pulsat
        candau[boto] = !candau[boto]
        boto = lector.nextInt()
    }

    println(candau)

}