package cat.itb.marcvives.daw.m03.uf1.iterative

import java.util.*

fun main() {
    val lector = Scanner(System.`in`)
    val num1 = lector.nextInt()
    val num2 = lector.nextInt()

    for (i in num1 + 1 until num2) {
        print(i)
    }
}