package cat.itb.marcvives.daw.m03.uf1.iterative

import java.util.*

fun main() {
    var lector = Scanner(System.`in`)
    val numRepeticions = lector.nextInt()

    var contador = 1
    repeat(numRepeticions) {
        print(contador)
        contador++ //contador += 1    contador = contador +1
    }
}