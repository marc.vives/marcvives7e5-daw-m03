package cat.itb.marcvives.daw.m03.uf2.project.ui

import cat.itb.marcvives.daw.m03.uf2.project.model.AppState
import cat.itb.marcvives.daw.m03.uf2.project.model.FilmITB
import cat.itb.marcvives.daw.m03.uf2.project.model.User
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val ui = UI(scanner)
    ui.start()
}

class UI(val scanner: Scanner) {
    val filmITB = FilmITB()
    val state = AppState()
    val userUi = UserUI(scanner, filmITB, state)
    val filmUi = FilmUI(scanner, filmITB, state)
    val searchUi = SearchUi(scanner, filmITB, state)

    fun start() {
        println("Add a username")
        val newUser = User(scanner.nextLine())
        filmITB.addUser(newUser)
        println("Select username")
        val userSelected = User(scanner.nextLine())
        state.currentUser = userSelected

        var opcio = -1
        while (opcio != 0) {
            showMenu()
            opcio = scanner.nextInt()
            scanner.nextLine()
            executeOption(opcio)
        }
    }

    fun showMenu() {
        println("Welcome to FilmItb:")
        println("1: User")
        println("2: Films")
        println("3: Search")
        println("0: Exit")
    }

    fun executeOption(option: Int) {
        when (option) {
            1 -> userUi.start()
            2 -> filmUi.start()
            3 -> searchUi.start()
        }

    }
}