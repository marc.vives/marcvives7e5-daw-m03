package cat.itb.marcvives.daw.m03.uf4.exercices.shop


fun main() {

    val v1 = BicycleModel("Jett 24", 5)
    val v2 = BicycleModel("Hotwalk", 7)

    v1.imprimir()
    v2.imprimir()
}