package cat.itb.marcvives.daw.m03.uf4.enums.colors

fun main() {

    val rectangle1 = RectangleFigure(PrintColors.RED, 4, 5)
    val triangle1 = LeftPiramidFigure(PrintColors.YELLOW, 3)
    val rectangle2 = RectangleFigure(PrintColors.GREEN, 3, 5)

    val figures = mutableListOf<Figura>(rectangle1, triangle1, rectangle2)

    figures.forEach(){
        it.print()
    }
}