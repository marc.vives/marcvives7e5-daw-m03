package cat.itb.marcvives.daw.m03.uf1.lists

import cat.itb.marcvives.daw.m03.uf1.data.Rectangle
import java.util.*


fun main() {
    val lector = Scanner(System.`in`)

    val llistaEnters: List<Int> = listOf(1, 5, 3, 8)
    var llistaBooleans: List<Boolean> = listOf(true, true, false, true, false)
    llistaBooleans = listOf(false, false, false)

    val llistaRectangles: List<Rectangle> = listOf(Rectangle(4.1, 5.3), Rectangle(4.1, 3.2))

    val llista = List(5) { lector.nextInt() }
    println(llista)

}