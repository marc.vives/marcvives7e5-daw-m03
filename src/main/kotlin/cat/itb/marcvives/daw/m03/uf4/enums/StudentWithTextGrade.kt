package cat.itb.marcvives.daw.m03.uf4.enums

enum class Grade(){
    SUSPES,
    BÉ,
    NOTABLE,
    EXCELLENT
}
class Student(val name: String, val grade : Grade){
    override fun toString(): String {
        return "Student(name='$name', grade=$grade)"
    }
}

fun main() {
    val mar = Student("Mar", Grade.SUSPES)
    val joan = Student("Joan", Grade.EXCELLENT)

    println(mar)
    println(joan)
}