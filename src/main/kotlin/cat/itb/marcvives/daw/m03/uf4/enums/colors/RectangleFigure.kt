package cat.itb.marcvives.daw.m03.uf4.enums.colors

class RectangleFigure(color: PrintColors, val width: Int, val height: Int) : Figura(color) {

    override fun print(){
        super.prepareColor()
        repeat(height){
            repeat(width){
                print("X")
            }
            println()
        }
        super.clearColor()
    }

}