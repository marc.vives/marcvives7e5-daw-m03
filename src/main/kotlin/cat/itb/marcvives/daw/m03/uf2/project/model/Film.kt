package cat.itb.marcvives.daw.m03.uf2.project.model

import kotlinx.serialization.Serializable

@Serializable
data class Film(
    val title: String,
    val director: String,
    val mainActor: String,
    val genere: String,
    val length: Int
) {
}