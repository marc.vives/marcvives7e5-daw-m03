package cat.itb.marcvives.daw.m03.uf1.lists

fun main() {
    val list1 = mutableListOf('a', 'f')

    val listEnters = mutableListOf(10, 10, 30, 40, 50, 60, 70)

    println(listEnters)
    listEnters[1] = 20
    println(listEnters)


    println("num elements: ${listEnters.size}")
    println("últim valor: ${listEnters.last()}")

}

