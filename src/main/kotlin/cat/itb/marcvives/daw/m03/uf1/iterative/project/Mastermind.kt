package cat.itb.marcvives.daw.m03.uf1.iterative.project

import java.util.*

data class Evaluation(val rightPosition: Int, val wrongPosition: Int)

fun main() {
    val intentosMaximo = 12
    // Use only this scanner
    val scanner = Scanner(System.`in`)
    var secret = ""
    // Your code
    println("Vols jugar en mode 1vs1 (1) o solitari (2)?")
    val modeJoc = scanner.nextInt()

    when (modeJoc) {
        1 -> {
            println("Introdueix la paraula secreta")
            secret = scanner.next()
        }
        2 -> repeat(4) { secret += randomChar() }
    }

    println("Comença el joc!")
    var haGuanyat = false
    for(intento in 0 until 12) { //0 .. 11 // 1..12
        println("Introdueix una combinació")
        val paraulaUsuari = scanner.next()
        var evaluation = evaluateWord(secret, paraulaUsuari)

        if(evaluation.rightPosition==4) {
            println("Enhorabona! has guanyat ")
            haGuanyat = true
            break
        }
        println("Posicions correctes: ${evaluation.rightPosition} Posicions incorrectes: ${evaluation.wrongPosition}")

    }
    if(haGuanyat == false)
        println("Fi del joc. Has perdut!")
    //use this function
}

fun randomChar(): Char {
    val allowedChars = 'A'..'F'
    return allowedChars.random()
}

fun evaluateWord(secret: String, guess: String): Evaluation {
    var wrongPosition = 0;
    var rightPosition = 0;

    for (i in 0..secret.lastIndex) {
        //para cada letra de secret

        for (j in 0..guess.lastIndex) {
            //para cada letra de guess
            if (guess[j] == secret[i]) {
                if (i == j)
                    rightPosition++
                else
                    wrongPosition++
            }
        }

    }

    //Calculate your right and wrong positions and change it as you need
    return Evaluation(rightPosition, wrongPosition)
}
