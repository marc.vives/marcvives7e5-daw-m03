package cat.itb.marcvives.daw.m03.uf1.iterative

fun main() {

    for (fila in 1..9) {
        for (columna in 1..9) {
            print("${columna * fila}\t")
        }
        println()
    }

}