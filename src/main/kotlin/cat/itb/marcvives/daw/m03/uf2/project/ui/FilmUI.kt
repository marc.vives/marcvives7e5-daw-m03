package cat.itb.marcvives.daw.m03.uf2.project.ui

import cat.itb.marcvives.daw.m03.uf2.project.model.AppState
import cat.itb.marcvives.daw.m03.uf2.project.model.Film
import cat.itb.marcvives.daw.m03.uf2.project.model.FilmITB
import java.util.*

class FilmUI(val sc: Scanner, val filmITB: FilmITB, val state: AppState) {

    fun start() {
        do {
            showMenu()
            val opcio = sc.nextInt()
            sc.nextLine()
            executeOption(opcio)

        } while (opcio != 0)
    }

    fun showMenu() {
        println("Films:")
        println("1: Add film")
        /* println("2: Show films")
         println("3: Delete films")
         println("4: Watch films")
         println("5: View watched films")*/
        println("6: Add film to favorites")
        println("7: Show favorites")
        println("8: Show likes per film")
        println("0: Return to main menu")
    }

    fun executeOption(option: Int) {
        when (option) {
            1 -> addFilmOption()
            6 -> addFavoriteOption()
            7 -> showFavoritesOption()
            8 -> showLikesPerFilmOption()
        }

    }

    fun showLikesPerFilmOption() {
        val list = filmITB.getLikesPerFilm()
        println(list)
        /*
        for(l in list){
            println("${l.amountLikes} persones han afeit a fovorites la pelicula ${l.title}")

        }*/
    }

    fun showFavoritesOption() {
        println(filmITB.getFavorites(state.currentUser.userName))
    }

    fun addFavoriteOption() {
        //per cridar a la funció addFavorite de filmITB,
        //necessito passar-li el titol i el nom d'usuari
        println("Enter a favorite film")
        val favoriteTitle = sc.nextLine()
        filmITB.addFavorite(favoriteTitle, state.currentUser.userName)
    }


    fun addFilmOption() {
        println("Enter a film")
        val title = sc.nextLine()
        println("Enter a director")
        val autor = sc.nextLine()
        println("Enter a main actor")
        val mainActor = sc.nextLine()
        println("Enter a genere")
        val genero = sc.nextLine()
        println("Enter a length")
        val duracion = sc.nextInt()

        val newFilm = Film(title, autor, mainActor, genero, duracion)
        filmITB.addFilm(newFilm)
    }
}
