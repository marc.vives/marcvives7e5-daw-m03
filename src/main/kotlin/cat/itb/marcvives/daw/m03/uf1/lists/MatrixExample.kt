package cat.itb.marcvives.daw.m03.uf1.lists

fun main() {
    val matrix = mutableListOf(
        mutableListOf(11, 12, 13),
        mutableListOf(21, 22, 23, 24, 25), // <-
        mutableListOf(31, 32, 33)
    )

    val tipo : List<Int> = matrix[1]
    println(tipo)

    val cuartaLista = mutableListOf(41, 42, 43)
    matrix.add(cuartaLista)
    println(matrix[3])

    matrix[0].add(0, 10)

    println(matrix)
}