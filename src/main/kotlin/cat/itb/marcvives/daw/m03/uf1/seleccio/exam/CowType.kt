package cat.itb.marcvives.daw.m03.uf1.seleccio.exam

import java.util.*

//vedell, vaca, toro o bou.
fun main() {
    val lector = Scanner(System.`in`)
    println("edat de l'animal, el sexe (1 = mascle, 2 = femella) i si està capat (1 = no capat, 2 = capat)")
    val edad = lector.nextInt()
    val sexe = lector.nextInt()
    val capat = lector.nextInt()

    if (edad < 2) {
        println("vedell")
    } else {
        if (sexe == 1) {
            if (capat == 1)
                println("toro")
            if (capat == 2)
                println("bou")
        }
        if (sexe == 2)
            println("vaca")
    }
}