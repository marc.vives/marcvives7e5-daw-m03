package cat.itb.marcvives.daw.m03.uf4.exercices.instrumentsimulator

abstract class Instrument{
    abstract fun makeSounds(times: Int)
}

class Triangle(val resonance: Int): Instrument() {
    override fun makeSounds(times: Int) {
        repeat(times) {
            //podriem fer un when ja que l'enuciat diu que seran valors de 1 a 5
            print("T")
            repeat(resonance) { print("I") }
            println("NC")
        }
    }

}

class Drump(val type: String): Instrument() {
    override fun makeSounds(times: Int) {
        repeat(times) {
            when (type) {
                "A" -> println("TAAAM")
                "O" -> println("TOOOM")
                "U" -> println("TUUUM")
            }
        }
    }
}

fun main() {
    val instruments: List<Instrument> = listOf(
        Triangle(5),
        Drump("A"),
        Drump("O"),
        Triangle(1),
        Triangle(5)
    )
    play(instruments)
}

private fun play(instruments: List<Instrument>) {
    for (instrument in instruments) {
        instrument.makeSounds(2) // plays 2 times the sound
    }
}