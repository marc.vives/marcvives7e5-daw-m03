package cat.itb.marcvives.daw.m03.uf1.data

import java.util.*

/*
* A un trabajador le pagamos las horas normales a 20€
* y las extra un 50% alto
 */
fun main() {
    val lector = Scanner(System.`in`)

    val precioHora = 20
    val precioHoraExtra = precioHora + precioHora * 0.5
    val salarioTotal: Double
    print("Quantas horas normales: ")
    val horasNormales = lector.nextInt()
    print("Quantas horas extras: ")
    val horasExtra = lector.nextInt()

    salarioTotal = horasNormales * precioHora + horasExtra * precioHoraExtra

    println(salarioTotal)
}