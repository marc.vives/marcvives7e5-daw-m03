package cat.itb.marcvives.daw.m03.uf4

data class Cotxe(val matricula: String,
                 val numPortes: Int)


fun main() {
    val v1 = Cotxe("1212AAA", 5)
    println(v1.toString())
}