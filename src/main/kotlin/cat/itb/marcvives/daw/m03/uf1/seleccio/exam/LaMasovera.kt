package cat.itb.marcvives.daw.m03.uf1.seleccio.exam

import java.util.*

fun main() {
    val lector = Scanner(System.`in`)
    val dia = lector.nextLine()

    when (dia) {
        "dilluns" -> println("Compra llums")
        "dimarts" -> println("Compra naps")
        "dimecres" -> println("Compra nespres")
        "dijous" -> println("Compra nous")
        "divendres" -> println("Faves tendres")
        "dissabte" -> println("Tot s'ho gasta")
        "diumenge" -> println("Tot s'ho menja")
        else -> println("Error!")
    }
}