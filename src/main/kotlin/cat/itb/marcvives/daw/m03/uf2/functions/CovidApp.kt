package cat.itb.marcvives.daw.m03.uf2.functions

import countTotalCases
import growthRates
import readDailyCasesFromScanner
import java.util.*
import kotlin.math.round

fun main() {

    val scanner = Scanner(System.`in`)

    val casosDiaris = readDailyCasesFromScanner(scanner)
    val casosTotal = countTotalCases(casosDiaris)
    println("Hi ha hagut $casosTotal casos en total.")

    val creixements = growthRates(casosDiaris)
    val ultimCreixement2D = round(creixements.last()*100)/100
    println("L'útlim creixement és de $ultimCreixement2D")

    val mitjaCreixements = average(creixements)
    val mitjaCreixements2Decimals = round(mitjaCreixements*1000)/1000
    println("La mitjana de creixement és de $mitjaCreixements2Decimals")

}