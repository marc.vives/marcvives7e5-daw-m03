package cat.itb.marcvives.daw.m03.uf1.functions

import java.util.*
import kotlin.math.pow
import kotlin.math.sqrt

fun main() {
    val lector = Scanner(System.`in`).useLocale(Locale.US)
    val amplada = lector.nextDouble()
    val alçada = lector.nextDouble()
    val camiCurt = shortestPathOfField(amplada, alçada)

    println(camiCurt)
}

// aquesta funció rebra dos Double's, el primer li direm width i el segon height
fun shortestPathOfField(width: Double, height: Double): Double {
    val sumaQuadrats = width.pow(2) + height.pow(2)
    val result: Double = sqrt(sumaQuadrats)
    return result       // fixar-se amb el ": Double" de la linia 17
}