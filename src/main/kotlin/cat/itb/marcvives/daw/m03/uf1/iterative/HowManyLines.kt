package cat.itb.marcvives.daw.m03.uf1.iterative

import java.util.*

fun main() {

    var contador = 0
    val lector = Scanner(System.`in`)
    var lineaText = lector.nextLine()

    //mentre la linia sigui diferent a END
    // - incrementar contador
    // - llegeixo una altra línea

    while (!lineaText.equals("END")) {
        contador++
        lineaText = lector.nextLine()
    }

    println(contador)
}