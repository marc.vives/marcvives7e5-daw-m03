package cat.itb.marcvives.daw.m03.uf4.herencia

open class Figura(val color: String) {

    open fun print(){}

    fun prepareColor(){
        when (color) {
            "BLACK" -> print(BLACK)
            "RED" -> print(RED)
            "GREEN" -> print(GREEN)
            "YELLOW" -> print(YELLOW)
            "BLUE" -> print(BLUE)
            "PURPLE" -> print(PURPLE)
            "CYAN" -> print(CYAN)
            "WHITE" -> print(WHITE)
        }
    }

    fun clearColor(){
        print(RESET)
    }
}