package cat.itb.marcvives.daw.m03.uf2.modularitat

data class Board(val n: Int, val numBombes: Int){

    var matrix = MutableList(n) { i -> MutableList(n) { j -> Cell(i, j) } }

    fun addBombs(cells: MutableList<Cell>){
        for(cell in cells){
            matrix[cell.i][cell.j].bomba = true
        }
    }

    fun generateBombsList():MutableList<Cell>{
        val bombList = mutableListOf<Cell>()
        while(bombList.size < numBombes){
            val i = (0 until n).random()
            val j = (0 until n).random()
            val cell = Cell(i, j)
            if(!bombList.contains(cell)){
                bombList.add(cell)
            }
        }
        return bombList
    }

    fun calculateAdjacencies(){
        inicialitzarAdjacentProperty()
        for (i in 0 until n) {
            for (j in 0 until n) {
                //per sobre
                if(i>0 && j>0 && matrix[i-1][j-1].bomba) matrix[i][j].adjacents++
                if(i>0 && matrix[i-1][j].bomba) matrix[i][j].adjacents++
                if(i>0 && j<(n-1) && matrix[i-1][j+1].bomba) matrix[i][j].adjacents++
                //per sota
                if(i<(n-1) && j>0 && matrix[i+1][j-1].bomba) matrix[i][j].adjacents++
                if(i<(n-1) && matrix[i+1][j].bomba) matrix[i][j].adjacents++
                if(i<(n-1) && j<(n-1) && matrix[i+1][j+1].bomba) matrix[i][j].adjacents++
                //als costats
                if(j>0 && matrix[i][j-1].bomba) matrix[i][j].adjacents++
                if(j<(n-1) && matrix[i][j+1].bomba) matrix[i][j].adjacents++
            }
        }
    }

    fun showInformation(): String{
        var info = ""
        for(i in 0 until n){
            for(j in 0 until n){
                info+= "${matrix[i][j].showInformation()} "
            }
            info += "\n"
        }
        return info
    }

    fun showHiddenInformation(): String{
        var info = ""
        for(i in 0 until n){
            for(j in 0 until n){
                info+= "${matrix[i][j].showHidedCells()} "
            }
            info += "\n"
        }
        println(info)
        return info
    }



    private fun inicialitzarAdjacentProperty() {
        //recorro tota la matriu
        for (i in 0 until n) {
            for (j in 0 until n) {
                matrix[i][j].adjacents = 0

            }
        }
    }

}