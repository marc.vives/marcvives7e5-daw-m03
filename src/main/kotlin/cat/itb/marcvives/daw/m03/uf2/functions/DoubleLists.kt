package cat.itb.marcvives.daw.m03.uf2.functions

fun average(list: List<Double>): Double {
    var suma = 0.0

    for (valor in list){
        suma += valor
    }

    return suma/list.size
}