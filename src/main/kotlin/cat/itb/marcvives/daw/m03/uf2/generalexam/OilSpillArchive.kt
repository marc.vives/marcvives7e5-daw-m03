package cat.itb.marcvives.daw.m03.uf2.generalexam

class OilSpillArchive(val oilSpills: MutableList<OilSpill> = mutableListOf<OilSpill>()){

    fun addOilSpill(oilSpill: OilSpill){
        oilSpills.add(oilSpill)
    }

    fun getAllSpills():MutableList<OilSpill>{
        return oilSpills
    }

    fun getWorstSpill():OilSpill?{
        if(oilSpills.isEmpty())
            return null

        //else
        var worstSpill = oilSpills.first()
        for (spill in oilSpills){
            if(spill.gravity > worstSpill.gravity){
                worstSpill = spill
            }
        }
        return worstSpill
    }

    fun getOilSpill(name: String): OilSpill? {
        for (spill in oilSpills) {
            if (spill.name == name) {
                return spill
            }
        }
        return null
    }

    fun getOilSpills(company: String): MutableList<OilSpill>{
        val spills = mutableListOf<OilSpill>()

        for(spill in oilSpills){
            if(spill.company == company)
                spills.add(spill)
        }
        return spills
    }

    fun getWorstCompany():String{
        return "Pendent d'implementació"
    }
}