package m03.uf2

import java.nio.file.Path
import java.util.*
import kotlin.io.path.*

fun main() {
    val scanner = Scanner(System.`in`)
    val rutaText = scanner.nextLine()
    var path : Path = Path(rutaText)


    println("exist : "+path.exists())
    val esAbs = path.isAbsolute
    println("isAbs : "+esAbs)
    println("isDir : "+path.isDirectory())
    println("isFile: "+path.isRegularFile())

    if(!esAbs){
        path = path.toAbsolutePath()
        println("path    : "+path)
        println("absoluta: "+path.toAbsolutePath())
    }
    println(path.parent)
    //println(path.resolve(Path("html/index.html")))


}