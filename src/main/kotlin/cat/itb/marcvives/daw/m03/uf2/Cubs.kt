package cat.itb.marcvives.daw.m03.uf2

fun main() {
    val figura1 = Cub(2, 3, 4)

    figura1.imprimir()
    println("volum: ${figura1.volum}")
    figura1.canviarFondo(10)
    figura1.imprimir()
    println("volum: ${figura1.volum}")
}

data class Cub(var base: Int, var altura: Int, var fondo: Int){
    val volum get() = base*altura*fondo

    fun imprimir(){
        println("base:$base - altura:$altura - fondo:$fondo")
    }
    fun canviarFondo(fondo: Int){
        this.fondo = fondo
    }
}
