package cat.itb.marcvives.daw.m03.uf2.project.ui

import cat.itb.marcvives.daw.m03.uf2.project.model.AppState
import cat.itb.marcvives.daw.m03.uf2.project.model.FilmITB
import java.util.*

class SearchUi(val sc: Scanner, val filmITB: FilmITB, val state: AppState) {

    fun start() {
        do {
            showMenu()
            val opcio = sc.nextInt()
            sc.nextLine()

            executeOption(opcio)

        } while (opcio != 0)
    }

    fun showMenu() {
        println("Search methods:")
        println("1: By title")

        println("2: By director")
        println("3: By main actor")
        println("4: By genere")
        println("5: By length")

/*        println("6: Not watched")
        println("7: Recomended")
*/
        println("0: Return to main menu")
    }

    fun executeOption(option: Int) {
        when (option) {
            1 -> searchByTitleOption()
            2 -> searchByDirectorOption()
            3 -> searchByMainActorOption()
            4 -> searchByGenereOption()
            5 -> searchByMaxDurationOption()
        }
    }

    private fun searchByMaxDurationOption() {
        println("enter a (max) duration")
        println(filmITB.searchByMaxDuration(sc.nextInt()))
        sc.nextLine()
    }

    private fun searchByGenereOption() {
        println("enter a genere")
        println(filmITB.searchByGenere(sc.nextLine()))
    }

    private fun searchByMainActorOption() {
        println("enter an actor")
        println(filmITB.searchByMainActor(sc.nextLine()))
    }

    private fun searchByDirectorOption() {
        println("enter a director")
        println(filmITB.searchByDirector(sc.nextLine()))
    }

    private fun searchByTitleOption() {
        println("enter a title")
        println(filmITB.searchByTitle(sc.nextLine()))
    }


}