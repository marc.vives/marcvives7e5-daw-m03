package cat.itb.marcvives.daw.m03.uf4.figures

class Rectangle(val color: String, val width: Int, val height: Int) {

    fun paint() {
        when (color) {
            "BLACK" -> print(BLACK)
            "RED" -> print(RED)
            "GREEN" -> print(GREEN)
            "YELLOW" -> print(YELLOW)
            "BLUE" -> print(BLUE)
            "PURPLE" -> print(PURPLE)
            "CYAN" -> print(CYAN)
            "WHITE" -> print(WHITE)
        }

        repeat(width) {
            repeat(height) {
                print("X")
            }
            println()
        }
        print(RESET)
    }
}