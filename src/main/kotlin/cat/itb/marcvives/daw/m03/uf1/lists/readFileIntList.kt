package cat.itb.marcvives.daw.m03.uf1.lists

import java.util.*
import kotlin.io.path.Path


fun main() {
    val scanner = Scanner(Path("adventFiles/1_1.txt"));

    val list = mutableListOf<Int>()
    while(scanner.hasNext()){
        val number = scanner.nextInt()
        list += number // list.add(number)
    }

    println(list)
}