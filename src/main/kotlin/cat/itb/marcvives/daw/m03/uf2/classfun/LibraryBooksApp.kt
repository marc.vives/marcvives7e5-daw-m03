package cat.itb.marcvives.daw.m03.uf2.classfun

import java.util.*

fun main() {
    val lector = Scanner(System.`in`)
    val numLlibres = lector.nextInt()

    val books = MutableList(numLlibres){
        generaBook(lector)
    }

    mostrarTitols(books)
}

fun mostrarTitols(books: MutableList<Book>){
    for (book in books){
        println(book.title)
    }
}

fun generaBook(sc: Scanner): Book{
    println("Introdueix titol, autor i num de pagines")
    val title = sc.nextLine()
    val autor = sc.nextLine()
    val pages = sc.nextInt()
    sc.nextLine()
    return  Book(title,autor,pages)
}

data class Book(val title: String, val author: String, val pages: Int)