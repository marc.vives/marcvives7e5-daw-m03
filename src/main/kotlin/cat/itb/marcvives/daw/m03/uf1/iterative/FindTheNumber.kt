package cat.itb.marcvives.daw.m03.uf1.iterative

import java.util.*

val lector = Scanner(System.`in`)

fun main() {
    val numBuscat = 6//(0..10).random()
    val numMaximIntents = 5
    val intentsRealitzats = buscarNumero(numBuscat, numMaximIntents)
    println("El valor buscat era: $numBuscat - has utilitzat $intentsRealitzats intents")

}

fun buscarNumero(numBuscat: Int, numMaximIntents: Int): Int {
    for (intent in 1..numMaximIntents) {
        println("Intent $intent. Escriu un numero")
        val numUsuari = lector.nextInt()
        if (numUsuari == numBuscat) {
            println("Has encertat")
            return intent
        }
    }
    return -1
}