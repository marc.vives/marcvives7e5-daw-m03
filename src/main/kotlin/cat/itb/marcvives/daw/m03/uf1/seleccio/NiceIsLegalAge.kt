package cat.itb.marcvives.daw.m03.uf1.seleccio

import java.util.*

fun main() {
    val lector = Scanner(System.`in`)
    val age: Int = lector.nextInt()

    if (age >= 18) {
        println("ets major d'edat")
    }
}