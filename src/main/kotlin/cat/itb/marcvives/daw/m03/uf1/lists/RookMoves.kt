package cat.itb.marcvives.daw.m03.uf1.lists

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val tauler : List<MutableList<String>>

    tauler = List(8){
        MutableList(8){
            "X "
        }
    }

    val lletra = scanner.next()
    var columnaTorre = when(lletra){
        "a" -> 0
        "b" -> 1
        "c" -> 2
        "d" -> 3
        "e" -> 4
        "f" -> 5
        "g" -> 6
        "h" -> 7
        else -> -1
    }
    val filaTorre = scanner.nextInt()

    for(fila in 7 downTo 0){
        for(columna in 0 .. 7){
            if(fila == filaTorre-1 || columna == columnaTorre){
                tauler[fila][columna] = "♖ "
            }
            if(fila == filaTorre-1 && columna == columnaTorre){
                tauler[fila][columna] = "♜ "
            }
        }
    }
    println()


    for(fila in 7 downTo 0){
        for(columna in 0 .. 7){
            print(tauler[fila][columna])
        }
        println()
    }
    println()

}