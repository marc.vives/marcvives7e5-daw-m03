package cat.itb.marcvives.daw.m03.uf2.recursivity

import java.util.*

fun main() {
    val lector = Scanner(System.`in`)
    val numPunts = lector.nextInt()
    println(dotLineRecursive(numPunts))
}

fun dotLineRecursive(num: Int):String{
    if(num==0){
        return "FIN"
    }
    return "."+ dotLineRecursive(num-1)
}