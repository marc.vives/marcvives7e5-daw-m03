package cat.itb.marcvives.daw.m03.uf2.classfun

fun main() {
    val posicoInicial = Posicio(0.0, 0.0)
    val elMeuRobot = Robot(posicoInicial)
    //TODO: llegir acció i cridar a la funcio corresponent (tot en un while)
    elMeuRobot.pujar()
}

data class Posicio(var x: Double, var y: Double)

data class Robot(val posicio: Posicio, var velocitat: Double = 1.0){
    fun pujar(){
        posicio.y+=velocitat
    }

    //TODO: crear les funcions baixar, avançar, retrocedir, mostrar info...
}