package cat.itb.marcvives.daw.m03.uf1.iterative

import java.util.*

fun main() {

    while (true) {
        println("Bucle infinit");
    }
    val lector = Scanner(System.`in`)
    var numeroUsuari: Int

    do {
        //println("Introduieix un numero: ")
        numeroUsuari = lector.nextInt()
    } while (numeroUsuari !in 1..5)

    println("El número introduït: $numeroUsuari")
}