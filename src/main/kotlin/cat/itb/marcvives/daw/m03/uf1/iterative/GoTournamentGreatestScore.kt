package cat.itb.marcvives.daw.m03.uf1.iterative

import java.util.*

fun main() {
    val lector = Scanner(System.`in`)
    var guanyador = lector.nextLine()
    var puntMax = lector.nextInt()
    lector.nextLine()

    var jugador = lector.nextLine()
    while (jugador != "END") {
        val punts = lector.nextInt()
        lector.nextLine()

        if (punts > puntMax) {
            puntMax = punts
            guanyador = jugador
        }
        jugador = lector.nextLine()
    }

    println("$guanyador: $puntMax")

}