package cat.itb.marcvives.daw.m03.uf3.exercices.serialitzable

import cat.itb.marcvives.daw.m03.uf2.project.model.Film
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

fun main() {
    var films = mutableListOf<Film>()
    val film: Film = Film(
        "Titol de la pelicula",
        "Director de la pelicula",
        "Actor principal",
        "Pelicula d'Acció",
        150
    )

    films.add(film)
    films.add(film)
    val text: String = Json.encodeToString(films)
    println(text)

    val encoder = Json {
        ignoreUnknownKeys = true
    }

    var filmList2 = encoder.decodeFromString<List<Film>>(text)


    filmList2.forEach {
        println(it)
    }
}