package cat.itb.marcvives.daw.m03.uf1.lists

fun main() {
    val matrix = listOf(
                        listOf(2, 5, 1, 6),
                        listOf(23, 52, 14, 36),
                        listOf(23, 75, 81, 64)
                    )
    var resultat = 0;

    for(llista in matrix){
        for(numero in llista){
            resultat += numero
        }
    }

    println("resultat = ${resultat}")
}