package cat.itb.marcvives.daw.m03.uf4.exercices.arm

fun main() {
    val robot = MechanicalArm()

    robot.toggle()
    println(robot.getStatus())
    robot.updateAltitude(3)
    println(robot.getStatus())
    robot.updateAngle(180)
    println(robot.getStatus())
    robot.updateAltitude(-3)
    println(robot.getStatus())
    robot.updateAngle(-180)
    println(robot.getStatus())
    robot.updateAltitude(3)
    println(robot.getStatus())
    robot.toggle()
    println(robot.getStatus())
}