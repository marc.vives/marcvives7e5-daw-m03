package cat.itb.marcvives.daw.m03.uf1.iterative

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val cantitat = scanner.nextInt()
    var vocalesResult = ""

    repeat(cantitat) {
        var lletra = scanner.next()
        if (lletra == "a" || lletra == "e" || lletra == "i"
            || lletra == "o" || lletra == "u") {
            vocalesResult += "$lletra "
        }

    }
    println(vocalesResult)
}