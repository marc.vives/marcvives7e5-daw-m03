package cat.itb.marcvives.daw.m03.uf2.recursivity

fun main() {
    println(getAppleSong(4))
}

fun getAppleSong(applesCount: Int):String{
    if(applesCount == 0)
        return ""
    val actual = getAppleSongStanza(applesCount)
    return actual + getAppleSong(applesCount - 1)
}

fun getAppleSongStanza(applesCount: Int) =
    """
        $applesCount pometes té el pomer, 
        de $applesCount una, de $applesCount una,
        $applesCount pometes té el pomer,
        de $applesCount una en caigué. 
        Si mireu el vent d'on vé 
        veureu el pomer com dansa, 
        si mireu el vent d'on vé
        veureu com dansa el pomer.
    """

