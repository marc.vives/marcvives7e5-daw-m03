package cat.itb.marcvives.daw.m03.uf1.iterative

import java.util.*

fun main() {
    val lector = Scanner(System.`in`)
    val num = lector.nextInt()
    val exponent = lector.nextInt()

    var resultat = 1

    for (exp in 1..exponent) {
        resultat *= num
    }
    println(resultat)
}