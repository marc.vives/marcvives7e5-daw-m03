package cat.itb.marcvives.daw.m03.uf1.data

import java.util.*

//L'usuari escriu un número enter. Printa per pantalla
// el número següent amb el format: "Després ve el 5".

fun main() {
    val lector = Scanner(System.`in`)
    val numUsuari = lector.nextInt()
    println("Després ve el ${numUsuari + 1}")
}