package cat.itb.marcvives.daw.m03.uf2.recursivity

import java.util.*

fun main() {
    val lector = Scanner(System.`in`)
    val num = lector.nextInt()

    println(factorial(num.toLong()))
}

fun factorial(num: Long): Long{
    if(num == 0L)
        return 1
    else
        return num * factorial(num-1)
}