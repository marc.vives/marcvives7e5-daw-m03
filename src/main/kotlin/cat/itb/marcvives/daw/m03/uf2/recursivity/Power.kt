package cat.itb.marcvives.daw.m03.uf2.recursivity

import java.util.*

fun main() {
    val lector = Scanner(System.`in`)
    val base = lector.nextInt()
    val potencia = lector.nextInt()

    println(power(base, potencia))
}

fun power(base: Int, potencia:Int):Int{
    if(potencia == 0 ){
        return 1
    }
     return base * power(base, potencia-1)
}