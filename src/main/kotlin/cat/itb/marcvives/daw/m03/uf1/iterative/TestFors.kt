package cat.itb.marcvives.daw.m03.uf1.iterative

fun main() {
    for (i in 1..4) print(i)
    println("--------------")

    for (i in 4 downTo 1) print(i)
    println("--------------")

    for (i in 1..8 step 2) print(i)
    println("--------------")

    for (i in 8 downTo 1 step 2) print(i)
    println("--------------")

    for (i in 1 until 10) {       // i in [1, 10), 10 is excluded
        // for (i in 1..10-1) {
        print(i)
    }
}