package cat.itb.marcvives.daw.m03.uf1.seleccio.exam

import java.util.*

//Imprimeix per pantalla el tipus de multa (Correcte, Multa lleu o Multa greu).
fun main() {
    val lector = Scanner(System.`in`)
    val velocitat = lector.nextInt()

    if (velocitat <= 120) {
        println("Correcte")
    } else if (velocitat <= 140) {
        println("Multa lleu")
    } else {
        println("Multa greu")
    }
}