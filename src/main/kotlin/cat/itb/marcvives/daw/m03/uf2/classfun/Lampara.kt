package cat.itb.marcvives.daw.m03.uf2.classfun

data class Lampara(var encesa: Boolean = false) {
    fun turnOff(){
        encesa = false
    }
    fun turnOn(){
        encesa = true
    }
    fun toggle(){
        encesa = !encesa
    }

}