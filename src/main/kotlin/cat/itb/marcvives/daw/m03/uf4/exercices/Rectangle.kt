package cat.itb.marcvives.daw.m03.uf4.exercices

class Rectangle(val height:Double, val width:Double){
    val area get() = height * width
}