package cat.itb.marcvives.daw.m03.uf1.functions

import java.util.*

class Rectangle(val lenght: Double, val width: Double)

// Exemple amb passant un parametre d'entrada (un Rectangle)
fun calculateArea(rect1: Rectangle): Double {
    return rect1.lenght * rect1.width
}

// Exemple amb passant un parametre d'entrada (un Scanner)
fun readRectangle(lector: Scanner): Rectangle {
    val length = lector.nextDouble()
    val width = lector.nextDouble()
    val figura = Rectangle(length, width)
    return figura
}

fun main() {
    val lector = Scanner(System.`in`)
    val rect1 = readRectangle(lector)      // crida a la funció passant-li el lector
    val area1 = calculateArea(rect1)       // crida a calculateArea passant-li el rec1
    println("L'area del rectangle 1 és: $area1")

    val rect2 = readRectangle(lector)
    val area2 = calculateArea(rect2)       // crida a calculateArea passant-li el rec2
    println("L'area del rectangle 2 és: $area2")
}