package cat.itb.marcvives.daw.m03.uf2.classfun.CampSitePackage

data class CampManager(var reserves: MutableList<Reserva>){

    fun registrarEntrada(reserva: Reserva){
        reserves.add(reserva)
    }

    fun registrarSortida(nom: String){
        for(reserva in reserves){
            if(nom == reserva.nombre){
                reserves.remove(reserva)
                break
            }
        }
    }

    fun nomsReserva(): List<String>{
        val noms = List(reserves.size){
            reserves[it].nombre
        }
        return noms
    }


    fun numeroOcupants(): Int{
        var ocupants =0
        for(reserva in reserves){
            ocupants+=reserva.numeroOcupants
        }
        return ocupants
    }


}