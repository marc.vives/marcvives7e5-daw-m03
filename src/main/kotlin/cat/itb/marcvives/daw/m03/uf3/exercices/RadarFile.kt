package cat.itb.marcvives.daw.m03.uf3.exercices

import java.util.*
import kotlin.io.path.Path
import kotlin.io.path.exists
import kotlin.io.path.isRegularFile

fun main() {
    val scanner = Scanner(System.`in`)
    val filePath = Path(scanner.nextLine())
    if (filePath.exists() && filePath.isRegularFile()) {
        val lectures = mutableListOf<Int>()
        val scFile = Scanner(filePath)
        while (scFile.hasNextInt()) {
            lectures.add(scFile.nextInt())
        }

        println("Velocitat màxima: ${maxim(lectures)}km/h\n" +
                "Velocitat mínima: ${minim(lectures)}km/h\n" +
                "Velocitat mitjana: ${average(lectures)}km/h\n")
    }
}

fun average(list: List<Int>): Double {
    var suma = 0.0
    list.forEach { suma += it }
    return suma / list.size
}

fun maxim(list: List<Int>): Int {
    var max = list.first()
    list.forEach {
        if (it > max)
            max = it
    }
    return max
}

fun minim(list: List<Int>): Int {
    var min = list.first()
    list.forEach {
        if (it < min)
            min = it
    }
    return min
}