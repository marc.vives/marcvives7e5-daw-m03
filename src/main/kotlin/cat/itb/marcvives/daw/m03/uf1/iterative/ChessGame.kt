package cat.itb.marcvives.daw.m03.uf1.iterative

fun main() {

    val blanc = "   "
    val negre = "[■]"
    //fer 8 files
    for (fila in 1..8) {
        //dibuixa una fila
        for (columna in 1..8) {
            if ((fila + columna) % 2 == 0)
                print(blanc)
            else
                print(negre)
        }
        println()
    }
}