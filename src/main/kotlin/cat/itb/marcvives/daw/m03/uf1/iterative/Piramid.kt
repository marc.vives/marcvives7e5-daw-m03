package cat.itb.marcvives.daw.m03.uf1.iterative

import java.util.*

fun main() {
    val lector = Scanner(System.`in`)
    val amplada = lector.nextInt()

    for (linia in 1..amplada) {
        for (columna in 1..linia) {
            print("# ")
        }
        println()
    }

}