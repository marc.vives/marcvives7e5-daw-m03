package cat.itb.marcvives.daw.m03.uf2.modularitat

data class Game(val n: Int, val numBombes:Int) {
    val tauler = Board(n, numBombes)
    var numDestapades = 0
    init {
        tauler.addBombs(tauler.generateBombsList())
        tauler.calculateAdjacencies()
        //tauler.showHiddenInformation()
    }

}