package cat.itb.marcvives.daw.m03.uf2.functions

import java.util.*

fun readIntList(scanner: Scanner): List<Int> {
    val llista = mutableListOf<Int>()

    var num = scanner.nextInt()
    while (num != -1) {
        llista.add(num)
        num = scanner.nextInt()
    }

    return llista
}

fun min(list: List<Int>): Int {
    var min = list[0]

    for (numero in list) {
        if (numero < min) {
            min = numero
        }
    }
    return min
}

fun max(list: List<Int>): Int{
    var max = list[0]

    for (numero in list) {
        if (numero > max) {
            max = numero
        }
    }
    return max
}

fun avg(list: List<Int>): Double{
    var suma = 0
    for (numero in list){
        suma+=numero
    }

    return suma.toDouble()/list.size
}








