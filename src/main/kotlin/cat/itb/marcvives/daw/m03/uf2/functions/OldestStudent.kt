package cat.itb.marcvives.daw.m03.uf2.functions

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val edats = readIntList(scanner)

    val edatMesGran = max(edats)

    println("L'alumne més gran té $edatMesGran anys")
}