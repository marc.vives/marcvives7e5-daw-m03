package cat.itb.marcvives.daw.m03.uf4.herencia

import java.time.LocalDate

open class Person(var name : String,
                  val birthDate: LocalDate){
    open fun whatAmI() = "I'm a person"

    override fun toString(): String {
        return "Person(name='$name', birthDate=$birthDate)"
    }

}
class Student(name: String,
              birthDate: LocalDate,
              val startYear: Int
                ) : Person(name, birthDate){

    override fun whatAmI() = "I'm a student"
    override fun toString(): String{
        return "im ${super.name} from $startYear"
    }
}


fun main() {
    val person = Student("nom", LocalDate.now(), 2020)

    if (person is Student) {
        println(person.startYear)
    }
    if (person !is Person) {
        // person.startYear <- Invalid
        print("Not a Student")
    }

}