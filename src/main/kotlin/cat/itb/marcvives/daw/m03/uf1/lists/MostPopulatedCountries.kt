package cat.itb.marcvives.daw.m03.uf1.lists

import java.util.*

class Country(val nom : String, var poblacio : Int)

fun main() {
    val lector = Scanner(System.`in`)
    val quantitatPaisos = lector.nextInt()
    lector.nextLine()

    val paisos: MutableList<Country> = mutableListOf()
    repeat(quantitatPaisos) {
        val nom = lector.nextLine()
        val poblacio = lector.nextInt()
        lector.nextLine()
        paisos.add(Country(nom, poblacio))
    }

    val poblacioMinima = lector.nextInt()

    for(pais in paisos){
        if(pais.poblacio > poblacioMinima){
            println(pais.nom)
        }
    }

}