package cat.itb.marcvives.daw.m03.uf3.exercices

import java.nio.file.Path
import java.util.*
import kotlin.io.path.*

fun main() {
    val lector = Scanner(System.`in`)
    val pathUser = Path(lector.nextLine())

    val paths = pathUser.listDirectoryEntries()
    for (path in paths){
        if(!path.isDirectory())
            capitalizeInitial(path)
    }
}

fun capitalizeInitial(path: Path){
    //println("path = ${path}")
    val newName = path.name.capitalize()

    val newPath = path.parent.resolve(newName)
    //println("newPath = ${newPath}")
    path.moveTo(newPath)
}