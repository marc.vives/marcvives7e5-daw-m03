package cat.itb.marcvives.daw.m03.uf2.functions

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)

    val temperatures = readIntList(scanner)

    val mitja = avg(temperatures)
    println("Ha fet $mitja graus de mitjana")
}