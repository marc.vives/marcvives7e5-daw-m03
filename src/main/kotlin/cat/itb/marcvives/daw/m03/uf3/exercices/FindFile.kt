package cat.itb.marcvives.daw.m03.uf3.exercices

import java.util.*
import kotlin.io.path.Path

fun main() {

    val lector = Scanner(System.`in`)
    val path = Path(lector.nextLine())
    val fileName = lector.nextLine()

    path.toFile().walk().forEach {
        if(it.isFile && it.name == fileName){
            println(it)
        }
    }
}