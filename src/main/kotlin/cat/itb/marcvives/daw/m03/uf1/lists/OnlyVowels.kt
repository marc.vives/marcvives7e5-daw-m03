package cat.itb.marcvives.daw.m03.uf1.lists

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)

    val vocals = listOf('a', 'e', 'i', 'o', 'u')
    var vocalesResult = ""

    var frase = scanner.next()
    for (lletra in frase) {
        if (lletra in vocals) {
            vocalesResult += "$lletra "
        }
    }
    println(vocalesResult)
}