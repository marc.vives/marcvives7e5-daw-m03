package cat.itb.marcvives.daw.m03.uf2.modularitat

import java.util.*

data class UI(val game: Game){

    val scanner = Scanner(System.`in`)

    fun readCell():Cell{
        val cell = Cell(scanner.nextInt(), scanner.nextInt())
        return cell
    }

    fun play(){
        val maximDestapades = (game.n*game.n)-game.numBombes
        var fail = false
        while(game.numDestapades < maximDestapades && !fail) {
            val cell = readCell()
            game.tauler.matrix[cell.i][cell.j].destapada = true
            if (game.tauler.matrix[cell.i][cell.j].showInformation() == "B") fail = true

            game.numDestapades++

            println(game.tauler.showInformation())
        }

        if(fail){
            println("Bomb!")
        }else{
            println("You win!")
        }
    }
}