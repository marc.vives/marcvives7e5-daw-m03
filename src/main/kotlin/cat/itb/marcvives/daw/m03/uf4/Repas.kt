package cat.itb.marcvives.daw.m03.uf4

class Person(val nom: String, val cognom: String){
    var edad = 0
    var parents: MutableList<Person> = mutableListOf()

   constructor(nom: String, cognom: String, edad: Int): this(nom, cognom){
        this.edad = edad
    }

    constructor(nom: String):this(nom, "")

    constructor(nom: String,
                cognom: String,
                anyNaixement: Int,
                anyReferencia :Int):this(nom, cognom){
        this.edad = anyReferencia-anyNaixement
    }

    constructor (parent1: Person, parent2: Person, nom : String):this(nom, parent1.cognom){
        this.parents.add(parent1)
        this.parents.add(parent2)
    }

    override fun toString(): String {
        return "Person(nom='$nom', cognom='$cognom', edad=$edad, parents=$parents)"
    }

}

fun main() {
    val p1 = Person("nom1", "cognom1")
    val p2 = Person("nom2", "cognom2", 5)
    val p3 = Person("nom3", "cognom3", 1900,1950 )
    val p4 = Person(p1, p2, "Pere")
    val p5 = Person("nom5")

    println(p1)
    println(p2)
    println(p3)
    println(p4)
    println(p5)


}