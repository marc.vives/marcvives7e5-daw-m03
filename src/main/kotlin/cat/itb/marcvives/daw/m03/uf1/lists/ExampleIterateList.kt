package cat.itb.marcvives.daw.m03.uf1.lists

fun main() {
    val llistaParaules = listOf("Hola","Hello","Bye")
    //Exemple: Index 3 out of bounds for length 3
    //println(llistaParaules[3])



    llistaParaules.forEach{
        println(it)
    }
    println("------------")

    llistaParaules.forEach{ paraula ->
        println(paraula)
    }
    println("------------")

    llistaParaules.forEachIndexed{ posicio, paraula ->
        println("$posicio - $paraula")
    }
    println("------------")

    for(paraula in llistaParaules){
        println(paraula)
    }
    println("------------")


    for((posicio, paraula) in llistaParaules.withIndex()){
        println("$posicio - $paraula")
    }
    println("------------")

    for(i in 0..llistaParaules.lastIndex){
        println(llistaParaules[i])
    }
    println("------------")
    for(i in llistaParaules.indices){
        println(llistaParaules[i])
    }
    println("------------")

}