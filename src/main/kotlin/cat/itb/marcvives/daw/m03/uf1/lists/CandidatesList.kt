package cat.itb.marcvives.daw.m03.uf1.lists

import java.util.*

fun main() {
    val lector = Scanner(System.`in`)
    val numCandidats = lector.nextInt()
    lector.nextLine()

    //creem la llista de candidats, llegint x vegades (numCandidats) del lector.
    val llistaCandidats = List(numCandidats) { lector.nextLine() }
    //println(llistaCandidats)

    var candidatSelecciont = lector.nextInt()

    while (candidatSelecciont != -1) {
        println(llistaCandidats[candidatSelecciont - 1])
        candidatSelecciont = lector.nextInt()
    }
}