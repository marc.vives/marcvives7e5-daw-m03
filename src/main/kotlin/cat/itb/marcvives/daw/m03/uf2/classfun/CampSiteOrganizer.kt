package cat.itb.marcvives.daw.m03.uf2.classfun

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val campSite = CampSite()
    var operation = scanner.next()
    while (operation != "END") {
        when (operation) {
            "ENTRA" -> clientCheckIn(scanner, campSite)
            "MARXA" -> clientCheckOut(scanner, campSite)
        }
        printCurrentData(campSite)
        operation = scanner.next()
    }
}

fun printCurrentData(campSite: CampSite) {
    println("parcel·les: ${campSite.parceles}")
    println("persones: ${campSite.personAmount}")
}

fun clientCheckOut(scanner: Scanner, campSite: CampSite) {
    val name = scanner.next()
    campSite.checkOut(name)
}

fun clientCheckIn(scanner: Scanner, campSite: CampSite) {
    val ammount = scanner.nextInt()
    val name = scanner.next()
    val novaReserva = Reserva(name, ammount)
    campSite.checkIn(novaReserva)
}


//Data classes
data class Reserva(val name: String, val amount: Int)

data class CampSite(val reservas: MutableList<Reserva> = mutableListOf()) {
    val parceles: Int get() = reservas.size
    val personAmount: Int
        get() {
            var total = 0
            for (reserva in reservas)
                total += reserva.amount
            return total
        }

    fun checkIn(name: String, ammount: Int) {
        reservas += Reserva(name, ammount)
    }

    fun checkIn(reserva: Reserva) {
        reservas += reserva
    }

    fun checkOut(name: String) {
        val position = findClient(name)
        reservas.removeAt(position!!)
    }

    fun findClient(name: String): Int? {
        for (i in reservas.indices) {
            if (reservas[i].name == name)
                return i
        }
        return null
    }
}

