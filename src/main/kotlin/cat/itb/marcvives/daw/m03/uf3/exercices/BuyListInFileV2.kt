package cat.itb.marcvives.daw.m03.uf3.exercices

import java.util.*
import kotlin.io.path.Path
import kotlin.io.path.exists

fun main() {
    val sc = Scanner(System.`in`).useLocale(Locale.UK)
    val homePath = Path(System.getProperty("user.home"))
    val compraFile = homePath.resolve("shopingList.json")

    var productes = mutableListOf<Product>()

    if (compraFile.exists()) {
        productes = getProductsFromJsonFile(compraFile)
    }

    var sortir = false
    while (! sortir) {
        val quantitat = sc.nextInt()
        if(quantitat == 0){
            val nom = sc.next()
            val preu = sc.nextDouble()
            sc.nextLine()
            addOrUpdateProduct(Product(nom,preu, quantitat), productes)
        }
        else {
            sortir=true
        }
    }

    printShopingList(productes)
    saveListProductInJsonFile(productes, compraFile)
}

fun addOrUpdateProduct(newProduct: Product, products: MutableList<Product>) {
    var isNew = true
    for (productInList in products) {
        if (productInList.name == newProduct.name) {
            productInList.quantity += newProduct.quantity
            productInList.price = newProduct.price
            isNew = false
        }
    }
    if (isNew) {
        products.add(newProduct)
    }
}
