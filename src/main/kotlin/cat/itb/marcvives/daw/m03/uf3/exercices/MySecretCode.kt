package cat.itb.marcvives.daw.m03.uf3

import java.nio.file.Path
import java.nio.file.StandardOpenOption
import java.util.*
import kotlin.io.path.writeText

fun main() {
    val lector = Scanner(System.`in`)
    val secretText = askForText(lector)
    val pathSecretFile = Path.of("secret.txt")
    saveText(secretText, pathSecretFile)
}

fun askForText(lector: Scanner):String{
    //println("Introdueix el text")
    return lector.nextLine()
}

fun saveText(text: String, ruta: Path){
    //Afegir opció de crear o mosrtar avisos
    ruta.writeText(text)
}

fun concatText(text: String, ruta: Path){
    ruta.writeText(text, options = arrayOf(StandardOpenOption.APPEND))
    ruta.writeText("\n", options = arrayOf(StandardOpenOption.APPEND))
}