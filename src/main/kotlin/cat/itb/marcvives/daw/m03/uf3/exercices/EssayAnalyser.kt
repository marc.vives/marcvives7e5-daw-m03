package cat.itb.marcvives.daw.m03.uf3.exercices

import java.nio.file.Path
import java.util.*
import kotlin.io.path.Path
import kotlin.io.path.exists

fun main() {

    val lector = Scanner(System.`in`)
    val filePath = Path(lector.nextLine())

    if(filePath.exists()){
        println("Número de línies: ${contarLinies(filePath)}")
        println("Número de paraules: ${contarParaules(filePath)}")
    }else{
        println("No existeix")
    }

}

fun contarLinies(file: Path):Int{
    var linies = 0
    val sc = Scanner(file)
    while(sc.hasNextLine()){
        linies++
        sc.nextLine()
    }
    return linies
}

fun contarParaules(file: Path):Int{
    var paraules = 0
    val sc = Scanner(file)
    while(sc.hasNext()){
        paraules++
        sc.next()
    }
    return paraules
}