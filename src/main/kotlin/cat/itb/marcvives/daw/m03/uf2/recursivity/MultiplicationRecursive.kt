package cat.itb.marcvives.daw.m03.uf2.recursivity

import java.util.*

fun main() {
    val lector = Scanner(System.`in`)
    val num1 = lector.nextInt()
    val num2 = lector.nextInt()

    println(multiplicaFake(num1, num2))
}

fun multiplicaFake (a: Int, b:Int):Int{
    if (b==0)
        return 0
    else
        return 0 + multiplicaFake(a, b-1)
}