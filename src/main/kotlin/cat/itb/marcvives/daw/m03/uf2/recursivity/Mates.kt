package cat.itb.marcvives.daw.m03.uf2.recursivity




fun divisioFake  (a: Int, b:Int):Int{
    if( a < b){
        return 0
    }
    return 1 + divisioFake((a-b), b)
}