package cat.itb.marcvives.daw.m03.uf1.lists

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val filasMatrix1 = scanner.nextInt()
    val columnasMatrix1 = scanner.nextInt()

    val matrix1 = List(filasMatrix1){
        List(columnasMatrix1){
            scanner.nextInt()
        }
    }
    val matrix2 = List(filasMatrix1){
        List(columnasMatrix1){
            scanner.nextInt()
        }
    }


    for(fila in 0 until filasMatrix1){
        for(columna in 0 until columnasMatrix1){
            val suma = matrix1[fila][columna] + matrix2[fila][columna]
            print("$suma ")
        }
        println()
    }


}