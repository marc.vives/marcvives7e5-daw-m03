package cat.itb.marcvives.daw.m03.uf1.iterative

/**
 * Dibuixa taules de multiplicar
 */
fun main() {
    for (taula in 1..10){//(i in 0 until 8)
        print("taula de ${taula}: ")
        for(columna in 1 .. 9) {
            print("\t${columna*taula} ")
        }
        println()
    }


/*
    //Dibuixa un tauler de 8 files amb 4 columnes
    for (fila in 0..7){//(i in 0 until 8)
        print("fila ${fila+1}: ")
        for(columna in 0 .. 3) {
            print(" ${columna+1} ")
        }
        println()
    }
*/


}