package cat.itb.marcvives.daw.m03.uf1.lists
import java.util.*

class Producte(val nom: String, val preu: Double)

fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)

    val numProductes = scanner.nextInt() //4

    val productes = mutableListOf<Producte>()
    repeat(numProductes){
        //Opció 1
        var nom = scanner.next()
        var preu = scanner.nextDouble()
        var producte = Producte(nom, preu)
        productes.add(producte)
        //Opció 2
        //productes.add(Producte(scanner.next(), scanner.nextDouble()))
    }
    for(prod in productes){
        println("El producte ${prod.nom} val ${prod.preu}€")
    }

}