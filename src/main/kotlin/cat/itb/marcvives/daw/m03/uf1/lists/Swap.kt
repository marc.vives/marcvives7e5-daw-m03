package cat.itb.marcvives.daw.m03.uf1.lists

import java.util.*

fun main() {
    val lector = Scanner(System.`in`)

    val list = MutableList(4) { lector.nextInt() }
    val ultimInt: Int = list[list.lastIndex] //list[3]

    list[list.lastIndex] = list[0] //list[3] = list[0]
    list[0] = ultimInt
}