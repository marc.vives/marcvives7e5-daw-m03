package cat.itb.marcvives.daw.m03.uf2.classfun

import kotlin.math.pow
import kotlin.math.sqrt

data class RightTriangle(val base: Double, val altura: Double){
    val perimetre get() = base + altura + sqrt(base.pow(2)+altura.pow(2))
    val area get() = base * altura / 2

    fun obtenirInformacio():String{
        return "Un triangle de ${this.base} x ${this.altura} " +
                "té ${this.area} d'area i ${this.perimetre} de perímetre."
    }
}