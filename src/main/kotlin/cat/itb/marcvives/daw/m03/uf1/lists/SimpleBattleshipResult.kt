package cat.itb.marcvives.daw.m03.uf1.lists

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)

    val tauler = MutableList(7) {
        MutableList(7) {
            "0"
        }
    }

    tauler[0][0] = "x"
    tauler[0][1] = "x"
    tauler[0][6] = "x"
    tauler[1][2] = "x"
    tauler[1][6] = "x"
    tauler[2][6] = "x"
    tauler[3][1] = "x"
    tauler[3][2] = "x"
    tauler[3][3] = "x"
    tauler[3][6] = "x"
    tauler[4][4] = "x"
    tauler[5][4] = "x"
    tauler[6][0] = "x"

    println(tauler[3])
    println()
    //recorro files
    for (fila in 0 until 7) {
        // recorro columnes
        for (columna in 0 until 7) {
            print(tauler[fila][columna]+" ")
        }
        println()
    }

    val fila = scanner.nextInt()
    val columna = scanner.nextInt()
    if(tauler[fila][columna] == "x"){
        println("tocat")
    }else{
        println("aigua")
    }


}