package cat.itb.marcvives.daw.m03.uf4.herencia

class LeftPiramidFigure(color: String, val base: Int) : Figura(color){

    override fun print(){
        super.prepareColor()

        for(pis in 0..base){
            repeat(pis){
                print("X")
            }
            println()
        }
        println()

        super.clearColor()
    }
}