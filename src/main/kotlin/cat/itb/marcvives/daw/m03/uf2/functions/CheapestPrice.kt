package cat.itb.marcvives.daw.m03.uf2.functions

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val preus = readIntList(scanner)

    val minim = min(preus)
    println("El producte més econòmic val: $minim€")
}