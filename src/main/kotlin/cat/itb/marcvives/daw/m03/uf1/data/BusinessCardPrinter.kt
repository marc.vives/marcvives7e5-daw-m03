package cat.itb.marcvives.daw.m03.uf1.data

import java.util.*

class Empleat(var nom: String, var cognom: String, var despatx: Int)

fun main() {
    val lector = Scanner(System.`in`)

    println("Introduce tu nombre")
    val nombre = lector.nextLine()

    println("Introduce tu apellido")
    val apellido = lector.nextLine()

    println("Introduce tu despacho")
    val despacho = lector.nextInt()

    val empleat1 = Empleat(nombre, apellido, despacho)
    println("Empleada: ${empleat1.nom} ${empleat1.cognom} - Despatx: ${empleat1.despatx}")

}