package cat.itb.marcvives.daw.m03.uf2.project.ui

import cat.itb.marcvives.daw.m03.uf2.project.model.AppState
import cat.itb.marcvives.daw.m03.uf2.project.model.FilmITB
import cat.itb.marcvives.daw.m03.uf2.project.model.User
import java.util.*

class UserUI(val sc: Scanner, val filmITB: FilmITB, val state: AppState) {

    fun start() {
        do {
            showMenu()
            val opcio = sc.nextInt()
            sc.nextLine()

            executeOption(opcio)

        } while (opcio != 0)
    }

    fun showMenu() {
        println("Users:")
        println("1: Add user")
        println("2: Show my user")
        println("3: View users")
        /*println("4: Update user")
        println("5: Delete user")
        println("6: Change User")
        println("7: Show statistics")*/
        println("0: Return to main menu")
    }

    fun executeOption(option: Int) {
        when (option) {
            1 -> addUserOption()
            2 -> println(state.currentUser.userName)
            3 -> println(filmITB.getAllUsers())
        }
    }

    fun addUserOption() {
        println("Add a username")
        filmITB.addUser(User(sc.nextLine()))
    }
}