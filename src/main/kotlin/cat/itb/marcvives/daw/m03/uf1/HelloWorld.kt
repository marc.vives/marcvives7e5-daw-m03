package cat.itb.marcvives.daw.m03.uf1

fun main() {
    helloWorld()
}

fun helloWorld() {
    println("Hello, world!")
}