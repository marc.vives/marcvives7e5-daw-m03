package cat.itb.marcvives.daw.m03.uf2.classfun

import java.util.*

fun main() {
    val lector = Scanner(System.`in`).useLocale(Locale.US)

    val llista : List<RightTriangle> = crearLlistaTriangles(lector)
    mostrarInformacioLlista(llista)
}

