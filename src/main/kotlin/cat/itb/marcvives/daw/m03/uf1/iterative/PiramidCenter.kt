package cat.itb.marcvives.daw.m03.uf1.iterative

import java.util.*

fun main() {
    val lector = Scanner(System.`in`)
    val num = lector.nextInt()

    for(i in (num-1) downTo 0){
        repeat(i) {
            print(" ")
        }
        repeat(num-i) {
            print("# ")
        }
        println()
    }

}