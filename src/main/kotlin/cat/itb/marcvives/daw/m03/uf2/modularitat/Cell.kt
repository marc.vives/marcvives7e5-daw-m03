package cat.itb.marcvives.daw.m03.uf2.modularitat

data class Cell(val i:Int, val j:Int){
    var adjacents = -1
    var bomba:Boolean = false
    var destapada:Boolean = false

    fun showInformation():String{
        var info = ""
        if(destapada){
            if(bomba){
                info = "B"
            }else{
                info = adjacents.toString()
            }
        }else{
            info = "X"
        }
        return info
    }

    fun showHidedCells():String{
        var info = ""
        if(bomba){
            info = "B"
        }else{
            info = adjacents.toString()
        }
        return info

    }
}