package cat.itb.marcvives.daw.m03.uf2.generalexam

data class OilSpill(var name : String, val company: String, val liters : Int, val toxicity: Double){

    val gravity get()  = toxicity * liters
    fun toOutput():String = "$name - $company - $liters - $toxicity - $gravity"
}
