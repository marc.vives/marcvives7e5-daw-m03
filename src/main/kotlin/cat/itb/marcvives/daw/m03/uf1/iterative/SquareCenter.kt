package cat.itb.marcvives.daw.m03.uf1.iterative

import java.util.*

fun main() {
    val lector = Scanner(System.`in`)
    val gran = lector.nextInt()     //10
    val petit = lector.nextInt()    //4

    val marge = (gran-petit)/2      //3

    dibuixaQuadrat(gran, petit, marge)
}

fun dibuixaQuadrat(big: Int, small: Int, margin: Int) {
    for (i in 1..big) {
        if (i <= margin || i > big - margin) {//(i not in marge+1..gran-marge)
            repeat(big) {
                print("o")
            }
            println()
        } else {
            for (o in 1..margin) {
                print("o")
            }
            for (x in 1..small) {
                print("x")
            }
            for (o in 1..margin) {
                print("o")
            }
            println()
        }
    }
}