package cat.itb.marcvives.daw.m03.uf1.lists

import cat.itb.marcvives.daw.m03.uf1.data.project.scanner
import java.util.*

fun main() {
    val lector = Scanner(System.`in`)

    val intList: List<Int> = List(10){
        scanner.nextInt()
    }

    var minValue = intList.first()
    var posicioMinim = 0
    for((index, valor) in intList.withIndex()){
        if(valor < minValue){
            minValue = valor
            posicioMinim = index
        }
    }

    println(minValue)
//    println("pos: $posicioMinim")


}