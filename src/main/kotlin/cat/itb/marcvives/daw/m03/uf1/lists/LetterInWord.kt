package cat.itb.marcvives.daw.m03.uf1.lists

import java.util.*

fun main() {
    val lector = Scanner(System.`in`)
    val paraula = lector.next()     //Hello
    val posicio = lector.nextInt()  //1

    //paraula seria com ['H','e','l','l','o']
    println(paraula[posicio])       //e
}