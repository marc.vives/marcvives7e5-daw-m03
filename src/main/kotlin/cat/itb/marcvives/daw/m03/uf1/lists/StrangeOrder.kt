package cat.itb.marcvives.daw.m03.uf1.lists

import java.util.*

fun main() {

    val llistaOriginal = mutableListOf<Int>()
    val llistaStrange = mutableListOf<Int>()
    val scanner = Scanner(System.`in`)

    var numUsuari = scanner.nextInt() //3

    while(numUsuari != -1){
        llistaOriginal.add(numUsuari)
        numUsuari = scanner.nextInt()
    }

    for(i in 0..llistaOriginal.lastIndex){
        if(i % 2 ==0){
            //index parell
            llistaStrange.add(0, llistaOriginal[i])
        }else{
            //index imparell
            llistaStrange.add(llistaOriginal[i])
        }
    }
    println(llistaStrange)

}