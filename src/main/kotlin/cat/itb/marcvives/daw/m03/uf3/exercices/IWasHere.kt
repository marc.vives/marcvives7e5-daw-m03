package cat.itb.marcvives.daw.m03.uf3.exercices

import java.nio.file.StandardOpenOption
import java.time.LocalTime
import java.util.Locale
import kotlin.io.path.Path
import kotlin.io.path.createFile
import kotlin.io.path.exists
import kotlin.io.path.writeText

fun main() {
    val home = System.getProperty("user.home")// -> /home/sjo
    val homePath = Path(home)

    val file = homePath.resolve("i_was_here.txt")

    if(!file.exists()){
        file.createFile()
    }

    val time = LocalTime.now().toString()
    val text = "I Was Here: $time\n"

    file.writeText(text, options = arrayOf(StandardOpenOption.APPEND))
}