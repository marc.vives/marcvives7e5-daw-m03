package cat.itb.marcvives.daw.m03.uf2.classfun

import java.util.*

fun main() {
    val lamp1 = Lampara(false)
    val scanner = Scanner(System.`in`)

    var accio = scanner.nextLine()
    while(accio != "END"){
        when (accio){
            "TURN OFF" -> lamp1.turnOff()
            "TURN ON" -> lamp1.turnOn()
            "TOGGLE" -> lamp1.toggle()
        }
        println(lamp1.encesa)

        accio = scanner.nextLine()
    }
}
