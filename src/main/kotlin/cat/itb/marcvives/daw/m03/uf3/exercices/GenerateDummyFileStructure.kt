package cat.itb.marcvives.daw.m03.uf3.exercices

import kotlin.io.path.Path
import kotlin.io.path.createDirectories


fun main() {
    val homePath : String = System.getProperty("user.home") //   /home/sjo
    val pathHome = Path(homePath)

    repeat(100){
        val pathDummyFile = pathHome.resolve("pathDummyFolder/${it+1}")
        //println(pathDummyFile)
        pathDummyFile.createDirectories()
    }
}