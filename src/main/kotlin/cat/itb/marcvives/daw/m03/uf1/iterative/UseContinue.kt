package cat.itb.marcvives.daw.m03.uf1.iterative

import java.util.*

fun main() {
    val lector = Scanner(System.`in`)
    val jumpNumber = lector.nextInt()

    for (i in 1..10) {
        if (i == jumpNumber) {
            continue
        }
        println(i)
    }
}