package cat.itb.marcvives.daw.m03.uf3.exercices

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlin.io.path.Path
import kotlin.io.path.exists
import kotlin.io.path.readText
import kotlinx.serialization.json.Json
import java.nio.file.Path
import java.util.*
import kotlin.io.path.writeText

@Serializable
data class Tree(@SerialName("nom_cientific") val cientificName: String,
                @SerialName("nom_barri") val neighbourhood: String?)

fun main() {

    val resourcesDir = Path("resources")
    val treeFile = resourcesDir.resolve("OD_Arbrat_Zona_BCN.json")

    if(treeFile.exists()) {
        val treeList: List<Tree> = getTreesFromFile(treeFile)
        val lector = Scanner(System.`in`)
        val cientificName = lector.nextLine()
        val filteredList = getTreeListByCientificName(treeList, cientificName)
        saveTreesInJsonFile(cientificName, filteredList)
        println(countByCientificName(treeList, cientificName))
    }else{
        println("No s'ha trobat el fitxer: "+treeFile)
    }
}

fun getTreeListByCientificName(treeList: List<Tree>, nomCerca: String): List<Tree> {
    val resultat = mutableListOf<Tree>()
    treeList.forEach{  arbre ->
        if(arbre.cientificName == nomCerca)
            resultat.add(arbre)
    }
    return  resultat
}

fun saveTreesInJsonFile(fileName: String, listTree: List<Tree>){
    val resourcesDir = Path("resources")
    val treeFile = resourcesDir.resolve("treeFiles").resolve("$fileName.json")
    val jsonTrees : String = Json.encodeToString(listTree)

    treeFile.writeText(jsonTrees)
}

fun countByCientificName(treeList: List<Tree>, cientificName: String): Int{
    var count = 0
    for(tree in treeList){
        if(tree.cientificName == cientificName)
            //println(tree)
            count++
    }
    return count
}

fun getTreesFromFile(treeFile: Path): List<Tree> {
    val treeJson = treeFile.readText()
    val encoder = Json {
        ignoreUnknownKeys = true
    }
    val treeList: List<Tree> = encoder.decodeFromString(treeJson)
    return treeList
}