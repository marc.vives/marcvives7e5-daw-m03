package cat.itb.marcvives.daw.m03.uf2.project.model

data class FilmITB(
    private val users: MutableList<User> = mutableListOf(),
    private val films: MutableList<Film> = mutableListOf()
) {

    /**
     * Afegeix un usuari a la llista
     * @param newUser Usuari que s'ha d'afegir
     */
    fun addUser(newUser: User) {
        users.add(newUser)
    }

    /**
     * Afegeix una pelicula a la llista
     *
     * @param newFilm pelicula que s'ha d'afegir
     */
    fun addFilm(newFilm: Film) {
        films.add(newFilm)
    }

    /**
     * Llista completa d'usuaris registrats
     *
     * @return llista de Users registrats
     */
    fun getAllUsers(): List<User> {
        return users
    }

    /**
     * Llista completa de pellicules registrades
     *
     * @return llista de pelicules registrats
     */
    fun getAllFilms(): List<Film> {
        return films
    }

    fun searchByTitle(title: String): MutableList<Film> {
        val resultat = mutableListOf<Film>()
        for (film in films) {
            if (film.title.contains(title)) {
                resultat.add(film)
            }
        }
        return resultat
    }

    fun searchByDirector(director: String): MutableList<Film> {
        val resultat = mutableListOf<Film>()
        for (film in films) {
            if (film.director.contains(director)) {
                resultat.add(film)
            }
        }
        return resultat
    }

    fun searchByMainActor(mainActor: String): MutableList<Film> {
        val resultat = mutableListOf<Film>()
        for (film in films) {
            if (film.mainActor.contains(mainActor)) {
                resultat.add(film)
            }
        }
        return resultat
    }

    fun searchByGenere(genere: String): MutableList<Film> {
        val resultat = mutableListOf<Film>()
        for (film in films) {
            if (film.genere.contains(genere)) {
                resultat.add(film)
            }
        }
        return resultat
    }

    fun searchByMaxDuration(maxDuration: Int): MutableList<Film> {
        val resultat = mutableListOf<Film>()
        for (film in films) {
            if (film.length <= maxDuration) {
                resultat.add(film)
            }
        }
        return resultat
    }

    fun addFavorite(title: String, userName: String) {
        //necessito el titulo de la peli i el nombre del usuario al cual le gusta la peli
        //He de buscar el "userName" dins de la llista "users"
        val posicioUsuari = searchUserPosition(userName)
        //He de buscar el "title" dins de la llista "films"
        val posicioFilm = searchFilmPosition(title)
        val peliculaFavorita = films[posicioFilm]
        users[posicioUsuari].favorites.add(peliculaFavorita)
    }

    fun getFavorites(userName: String): List<Film> {
        val posicioUsuari = searchUserPosition(userName)
        return users[posicioUsuari].favorites
    }

    fun getLikesPerFilm(): List<FilmLikes> {
        val result = mutableListOf<FilmLikes>()
        for (film in films) {
            var likes = 0
            for (user in users) {
                if (user.favorites.contains(film)) {
                    likes++
                }
            }
            val resume = FilmLikes(film.title, likes)
            result.add(resume)
        }
        return result
    }

    /**
     * Retorna la posició on apareix l'usuari dins la llista users
     * @param userName nom de l'usuari a buscar
     * @return Si no existeix: -1. Si existeix, la seva posició
     */
    fun searchUserPosition(userName: String): Int {
        for (i in users.indices) {  //in 0 .. users.lastIndex
            if (userName == users[i].userName) {
                return i
            }
        }
        return -1
    }

    /**
     * Retorna la posició on apareix la pelicula dins la llista fils
     * @param title titol de la pelicula a buscar
     * @return Si no existeix: -1. Si existeix, la posició de la pelicula
     */
    fun searchFilmPosition(title: String): Int {
        for (i in films.indices) {  //in 0 .. users.lastIndex
            if (title == films[i].title) {
                return i
            }
        }
        return -1
    }

}