package cat.itb.marcvives.daw.m03.uf4.herencia

fun main() {

    val rectangle1 =RectangleFigure("RED", 4, 5)
    val triangle1 = LeftPiramidFigure("YELLOW", 3)
    val rectangle2 =RectangleFigure("GREEN", 3, 5)

    val figures = mutableListOf<Figura>(rectangle1, triangle1, rectangle2)

    figures.forEach(){
        it.print()
    }
}