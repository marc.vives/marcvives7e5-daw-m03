package cat.itb.marcvives.daw.m03.uf2.classfun

import java.util.*

fun crearTriangle(scanner: Scanner): RightTriangle{
    val altura = scanner.nextDouble()
    val base = scanner.nextDouble()
    return RightTriangle(altura, base)
}

fun crearLlistaTriangles(scanner: Scanner):List<RightTriangle>{
    val numTriangles = scanner.nextInt()
    val llistaTriangles = List(numTriangles){
        crearTriangle(scanner)
    }
    return llistaTriangles
}

fun mostrarInformacio(triangle: RightTriangle){
    println(triangle.obtenirInformacio())
}

fun mostrarInformacioLlista(llistaTriangles: List<RightTriangle>) {
    for (triangle in llistaTriangles){
        mostrarInformacio(triangle)
    }
}

