package cat.itb.marcvives.daw.m03.uf1.functions

import java.util.*

fun main() {
    saludarUsuari()
}

fun saludarUsuari() {
    val lector = Scanner(System.`in`)
    println("Diguem el teu nom complet: ")
    val nomUsuari = lector.nextLine()
    println("Encat de coneixe't $nomUsuari")
}