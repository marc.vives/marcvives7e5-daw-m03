package cat.itb.marcvives.daw.m03.uf1.lists

import java.util.*

fun main() {

    val dies: List<String> = listOf("dilluns", "dimarts", "dimecres", "dijous", "divendres", "dissabte", "diumenge")

    val lector = Scanner(System.`in`)
    val index = lector.nextInt()

    println(dies[index])
}