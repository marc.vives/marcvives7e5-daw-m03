package cat.itb.marcvives.daw.m03.uf1.lists

import java.util.*

fun main() {

    //guardarem 11 valors, inicialment a 0
    val caixes = MutableList(11){ 0 }

    val lector = Scanner(System.`in`)
    var numCaixa = lector.nextInt()

    while (numCaixa != -1){
        //caixes[X] és el numero de vegades que
        // s'ha obert la caixa X
        caixes[numCaixa]++
        //caixes[numCaixa] = caixes[numCaixa]+1
        numCaixa = lector.nextInt()
    }
    println(caixes)

    for(i in 0 .. caixes.lastIndex){
        if(caixes[i]!=0)
            println("la caixa $i s'ha obert ${caixes[i]} vegades")
    }

}

