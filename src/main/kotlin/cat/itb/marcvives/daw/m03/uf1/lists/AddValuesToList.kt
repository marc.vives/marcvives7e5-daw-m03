package cat.itb.marcvives.daw.m03.uf1.lists

fun main() {
    val listFloats = MutableList(50) { 0.0f }

    //dentro de los corchetes tiene que ir un entero (posicion),
    //los indices/posiciones siempre son valores enteros
    listFloats[0] = 31.0f
    listFloats[1] = 56.0f
    listFloats[19] = 12.0f
    listFloats[listFloats.lastIndex] = 79.0f

    println(listFloats)

}