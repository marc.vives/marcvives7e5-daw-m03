package cat.itb.marcvives.daw.m03.uf1.iterative

import java.util.*

fun main() {
    val lector = Scanner(System.`in`)
    val numRep = lector.nextInt()

    for (i in 1..numRep) {
        print(".")
    }

}