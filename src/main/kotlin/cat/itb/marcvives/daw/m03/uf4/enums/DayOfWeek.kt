package cat.itb.marcvives.daw.m03.uf4.enums
import cat.itb.marcvives.daw.m03.uf4.enums.DayOfWeek.*

enum class DayOfWeek {
    SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY
}

fun main() {
    val dow : DayOfWeek = DayOfWeek.MONDAY
    println(tellItLikeItIs(dow))
}

fun tellItLikeItIs(dow: DayOfWeek) = when (dow) {
    MONDAY -> "Mondays are bad."
    TUESDAY, WEDNESDAY, THURSDAY -> "Midweek days are so-so."
    FRIDAY -> "Fridays are better."
    SATURDAY, SUNDAY -> "Weekends are best."
}


fun tellItLikeItIs2(dow: DayOfWeek) : String{
    when (dow) {
        MONDAY -> return "Mondays are bad."
        TUESDAY, WEDNESDAY, THURSDAY -> return "Midweek days are so-so."
        FRIDAY -> return "Fridays are better."
        SATURDAY, SUNDAY -> return "Weekends are best."
    }

}