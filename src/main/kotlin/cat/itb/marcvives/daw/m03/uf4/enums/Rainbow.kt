package cat.itb.marcvives.daw.m03.uf4.enums

import java.util.*

enum class Rainbow() {
    RED,    ORANGE,    YELLOW,    GREEN,    BLUE,
    INDIGO,    VIOLET
}

fun main() {
    val scanner = Scanner(System.`in`)
    val userColor = scanner.next()

    println(colorInRainbow(userColor))
}


fun colorInRainbow(color: String):Boolean{

    for(enum in Rainbow.values()){
        if(enum.name == color.uppercase())
            return true
    }

    return false
}