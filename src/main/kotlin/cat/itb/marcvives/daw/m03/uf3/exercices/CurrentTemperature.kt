package cat.itb.marcvives.daw.m03.uf3.exercices

import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*
import io.ktor.client.request.*
import kotlinx.serialization.SerialName
import kotlin.math.round

@kotlinx.serialization.Serializable
data class Wheather(val main: WheatherMain, val name: String, val weather: List<WheatherInfo>)

@kotlinx.serialization.Serializable
data class WheatherMain(@SerialName("temp") val tempKelvin: Double, val humidity: Int){
    val tempCelsius = round((tempKelvin-273.15)*100)/100
}

@kotlinx.serialization.Serializable
data class WheatherInfo(val id: Int, val main: String,  val description: String, val icon: String)

suspend fun main() {
    val apiUrl =
        "http://api.openweathermap.org/data/2.5/weather?lat=41.390205&lon=2.154007&appid=d662e754d0671e1384f22d2d9023795d"

    val client = HttpClient(CIO) {
        install(JsonFeature) {
            serializer = KotlinxSerializer(kotlinx.serialization.json.Json {
                ignoreUnknownKeys = true
            })
        }
    }
    val weather: Wheather = client.get(apiUrl)

    println(
        "Bon dia,\n" +
                "Avui fa ${weather.main.tempCelsius} a la ciutat de ${weather.name} amb una humitat del ${weather.main.humidity}%."
    )

    for (weatherItem in weather.weather) {
        println(weatherItem)
    }
}