package cat.itb.marcvives.daw.m03.uf2.recursivity

import java.util.*

fun main() {
    val lector = Scanner(System.`in`)
    val num = lector.nextInt()
    println(figures(num))
}
fun figures(num: Int):Int{
    if(num == 0)
        return 0
    else
        return 1 + figures(num/10)
}