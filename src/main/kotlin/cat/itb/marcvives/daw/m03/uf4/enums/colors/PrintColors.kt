package cat.itb.marcvives.daw.m03.uf4.enums.colors

enum class PrintColors(val color: String, val rgb: String, val consoleColor: String) {
    RED("Red", "#FF0000", "\u001b[0;31m"),
    YELLOW("Yellow", "#FFFF00", "\u001b[0;33m" ),
    GREEN("Green", "#00FF00", "\u001b[0;32m"),
    BLUE("Blue", "#0000FF", "\u001b[0;34m"),
    INDIGO("Indigo", "#4B0082", "\u001b[0;36m"),
    VIOLET("Violet", "#8B00FF", "\u001b[0;35m");
}