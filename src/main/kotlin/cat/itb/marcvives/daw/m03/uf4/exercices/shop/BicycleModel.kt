package cat.itb.marcvives.daw.m03.uf4.exercices.shop

class BicycleModel(val name: String, val  gears: Int,) {
    val brand = BycicleBrand("Specialized", "USA")

    fun imprimir(){
        println(toString())
    }

    override fun toString(): String {
        return "BicycleModel{name='$name', gears=$gears, brand=$brand}"
    }

    class BycicleBrand(val name: String, val country: String){
        override fun toString(): String {
            return "BycicleBrand{name='$name', country='$country'}"
        }
    }
}


