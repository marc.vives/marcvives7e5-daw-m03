package cat.itb.marcvives.daw.m03.uf4.exercices

fun main() {
    val rectangles = listOf(
        Rectangle(2.2, 4.5),
                            Rectangle(3.4, 1.5)
    )

    val tauler = Board(rectangles)
    println(tauler.countRectangles())
    println(tauler.getTotalArea())
}