package cat.itb.marcvives.daw.m03.uf2.generalexam

import java.util.*

fun main() {
    val sc = Scanner(System.`in`).useLocale(Locale.UK)
    val archive = OilSpillArchive()
    val ui = OilSpillArvhiceUi(sc, archive)
    ui.start()
}

data class OilSpillArvhiceUi(val sc: Scanner, val oilSpillArchive: OilSpillArchive) {
    fun start() {
        do {
            showMenu()
            val opcio = sc.nextInt()
            sc.nextLine()
            executaOpcio(opcio)
        } while (opcio != 0)
    }

    fun executaOpcio(option: Int) {
        when (option) {
            1 -> addOilSpill()
            2 -> showListOilSpills()
            3 -> showWorstSpill()
            4 -> showSpillLitters()
            5 -> showSpillGravity()
            6 -> spillsForCompany()
            7 -> showWorstCompany()
        }
    }

    fun addOilSpill(){
        val name =sc.nextLine()
        val company = sc.nextLine()
        val liters = sc.nextInt()
        val toxicity = sc.nextDouble()
        sc.nextLine()
        val oilSpill = OilSpill(name, company, liters, toxicity)
        oilSpillArchive.addOilSpill(oilSpill)
    }

    fun showListOilSpills() {
        println("name - company - litters - toxicity - gravity")
        val oilSpills = oilSpillArchive.getAllSpills()
        for(spill in oilSpills){
            println(spill.toOutput())
        }
    }

    fun showWorstSpill() {
        val worstSpill = oilSpillArchive.getWorstSpill()
        if (worstSpill != null) {
            println(worstSpill.toOutput())
        }
    }

    fun showSpillLitters() {
        val name = sc.nextLine()
        val spill = oilSpillArchive.getOilSpill(name)
        if(spill != null){
            println(spill.liters)
        }else{
            println("Can't find oil spill")
        }
    }

    fun showSpillGravity() {
        val name = sc.nextLine()
        val spill = oilSpillArchive.getOilSpill(name)
        if(spill != null) {
            println(spill.gravity)
        }else{
            println("Can't find oil spill")
        }
    }

    fun spillsForCompany() {
        val company = sc.nextLine()
        println("name - company - litters - toxicity - gravity")
        val spills = oilSpillArchive.getOilSpills(company)
        for (spill in spills){
            println(spill.toOutput())
        }
    }

    fun showWorstCompany() {

        println(oilSpillArchive.getWorstCompany())
    }

    fun showMenu() {
        println(
            "Chose option:\n" +
                    "1. Add oil spill\n" +
                    "2. List oil spills\n" +
                    "3. Worst oil spill\n" +
                    "4. Show Spill litters\n" +
                    "5. Show spill gravity\n" +
                    "6. Spills for company\n" +
                    "7. Worst company\n" +
                    "0. Exit"
        )
    }


}

