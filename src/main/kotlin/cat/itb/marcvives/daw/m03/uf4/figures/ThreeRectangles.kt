package cat.itb.marcvives.daw.m03.uf4.figures

fun main() {

    val r1 = Rectangle("RED", 4, 5)
    val r2 = Rectangle("YELLOW", 2, 2)
    val r3 = Rectangle("GREEN", 3, 5)

    r1.paint()
    println("------------------------")

    r2.paint()
    println("------------------------")

    r3.paint()
}