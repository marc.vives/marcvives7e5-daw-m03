package cat.itb.marcvives.daw.m03.uf1.data

import java.util.*

//L'usuari escriu el seu nom i printa per pantalla "Bon dia nom introduït"
fun main() {
    val lector = Scanner(System.`in`)
    val nomComplet = lector.nextLine()
    println("Bon dia $nomComplet")
}